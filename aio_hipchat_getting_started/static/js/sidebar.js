(function () {

    // Listen for incoming updates from the server (via SSE) and display
    // the received HTML in the sidebar container element

    $(document).ready(function () {
        console.log('ready');

        var $spinner = $("#spinner-container");
        $spinner.spin("medium");

        if (typeof(EventSource) == "undefined") {
            console.log('ERROR: Server-sent events not supported')
            $spinner.spin(false);
            document.getElementById("error-container").innerHTML = '<p>This browser does not support server side events</p>';
        } else {
            var baseUrl = $("meta[name=base-url]").attr("content");
            var evtSrc = new EventSource(baseUrl + "/sidebar-subscribe");

            evtSrc.onmessage = function (e) {
                var obj = JSON.parse(e.data);
                console.log(obj);

                if ($spinner !== null) {
                    $spinner.spin(false);
                    $spinner = null;
                }

                document.getElementById("sidebar-container").innerHTML = obj.html;
            };

            evtSrc.onerror = function (e) {
                console.log('ERROR:' + e.data);
                document.getElementById("error-container").innerHTML = '<p>ERROR:' + e.data + '</p>';
            };
        }
    });
})();
