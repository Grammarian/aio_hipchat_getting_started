import asyncio

import aiohttp_jinja2
from aio_hipchat.hc_addon_api import HcAddonApi
from aio_hipchat.hc_structures import ImageCard, Glance, GlanceContainer, CardNotification
from aio_hipchat.log import make_default_logger
from aio_hipchat.pusher import MultiPusher
from aio_hipchat.hc_client import HcClient

from .implementation import BrilliantImplementation

ADDON_KEY = 'simply-brilliant'
GLANCE_KEY = '%s.glance' % ADDON_KEY
SIDEBAR_KEY = '%s.sidebar' % ADDON_KEY

_logger = make_default_logger(__name__)


class SampleApi(HcAddonApi):

    api = HcAddonApi

    def __init__(self, app):
        super().__init__(app)

        app.installed.subscribe(self.post_install)
        app.uninstalled.subscribe(self.post_uninstall)

        # Create a cross-server pusher that will send html to all open sidebars
        # The path given must match the path used in sidebar.js for the EventSource
        self.multi_server_pusher = MultiPusher.createSse(self.app, "/sidebar-subscribe", ping_interval=10)

        app.register_on_finish(lambda x: self.multi_server_pusher.stop())


    @asyncio.coroutine
    def post_install(self, params):
        """
        This addon has just been installed. Let them know we are here
        """
        oauth = params['oauth']
        _logger.debug("Post processing install into room %d", oauth.room_id)

        ic = ImageCard(self.app.relative_to_base('/static/simple.png'),
                       side_by_side=True,
                       title="Welcome to simplicity",
                       link_url="http://objectlistview.sourceforge.net",
                       description='Simplicity addon has been installed. Coding addons will never be the same')

        hc_client = HcClient(oauth)
        yield from hc_client.send_room_notification(CardNotification(ic))

    @asyncio.coroutine
    def post_uninstall(self, params):
        """
        This addon has just been uninstalled. Clean up.

        At this point, the installation has already been deleted, so we can't actually do
        anything in HipChat itself.
        """
        oauth = params['oauth']
        _logger.debug("Post processing uninstall from room %d", oauth.room_id)

        # nothing to do at the moment

    # for any message that starts with '/simple', do something really interesting
    @api.message_hook('^/simple(\s|$)', "simple-command", auth='none', extract=True)
    def handle_simple_message_hook(self, request):

        # If extract=True on the message_hook, the request will be filled in with some commonly
        # used elements from the incoming request: 'body_as_json', 'message, 'sender', 'room'.

        # If auth='jwt', then some other properties will be sent on the request
        # * 'oauth'
        # * 'oauth_client_id'
        # * 'jwt_data'
        # * 'signed_request'
        # * 'token'

        # Of these, 'token' is probably the most important, since it can be used to make calls back
        # into HipChat to actually do something.

        hc_client = HcClient(request.oauth)

        c3 = ImageCard("http://www.skyscanner.net/static/sites/default/files/australia.jpg",
                       title="A nice title",
                       link_url="http://smh.com.au",
                       description=request.message.get('message')[3:],
                       image_url_2x="https://drscdn.500px.org/photo/116583489/m%3D2048/990100219838c055221df25c49b97b0d")

        yield from hc_client.send_room_notification(CardNotification(c3))

        # One feature of any message or webhook, is that if
        # we return a dictionary, it will be treated as parameters to a system notification
        # return {
        #     'message': 'this is the first message: ' + request.message.get('message')
        # }

    @api.webhook('room_enter')
    def handle_room_enter(self, request):
        yield from self.update_glance(request)
        yield from self.update_webpanel(request)
        # If we return nothing, it will be treated as HTTPNoContent, which is normally what we want

    @api.webhook('room_exit')
    def handle_room_exit(self, request):
        yield from self.update_glance(request)
        yield from self.update_webpanel(request)

    @api.glance(GLANCE_KEY, "Simple App", target=SIDEBAR_KEY, icon="/static/tick-in-circle.png")
    def handle_glance(self, request):
        """
        This method is only called once(!) and the result is cached until an api message is sent to update it.
        """
        glance = yield from self.generate_glance(request.oauth)
        return glance

    @api.webpanel(SIDEBAR_KEY, "Simple Data", icon="/static/simple.png")
    @aiohttp_jinja2.template('sidebar-outer.jinja2')
    def handle_webpanel(self, request):
        """
        Return the html that will be included in the iframe of the sidebar.

        This method is only invoked when the sidebar is opened by a user.
        Staying up to date after that is the responsibility of the sidebar.
        """
        yield from self.update_webpanel(request)
        return {
            "signed_request": request.token,
            "base_url": request.app.base_url
        }

    @asyncio.coroutine
    def generate_glance(self, oauth):

        # Glance can have either an extra icon or a lozenge -- not both

        bi = BrilliantImplementation(oauth)
        data = yield from bi.get_glance_data()
        if 'icon' in data:
            glance = Glance(data.get('label'),
                            icon_url=self.app.relative_to_base(data.get('icon')))
        else:
            glance = Glance(data.get('label'),
                            lozenge_text=data.get('lozenge_text'),
                            lozenge_type=data.get('lozenge_type'))
        return glance

    @asyncio.coroutine
    def update_glance(self, request):
        """
        Tell HipChat to update the glance used by this service
        """
        glance = yield from self.generate_glance(request.oauth)
        hc_client = HcClient(request.oauth)
        yield from hc_client.push_room_glance(GlanceContainer({GLANCE_KEY: glance}))

    @asyncio.coroutine
    def update_webpanel(self, request):
        """
        Generate the HTML that lives within the sidebar, and then push it to all
        servers and thence on to all clients.
        """
        bi = BrilliantImplementation(request.oauth)
        sidebar_data = yield from bi.get_sidebar_data(request)
        sidebar_html = aiohttp_jinja2.render_string("sidebar.jinja2", request, sidebar_data)
        _logger.debug('update_webpanel: %s', sidebar_html)
        yield from self.multi_server_pusher.push({'html': sidebar_html})


