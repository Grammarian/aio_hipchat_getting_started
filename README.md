A Simple HipChat Addon Example
==============================

This sample shows how easy it is to get up and running with a new addon to HipChat.

* Clone this getting started project

```sh
$ git clone https://Grammarian@bitbucket.org/Grammarian/aio_hipchat_getting_started.git
$ cd ./aio_hipchat_getting_started
```


* Make a new virtualenv and switch to it

```sh
$ virtualenv -p python3 venv
$ source venv/bin/activate
```


* Install all the required libraries into your new virtual environment:

```sh
$ pip install -r ./dev-requirements.txt 
```


* Look at the aio_hipchat_getting_started/app.py file. There is a app_config dictionary.
 You will eventually want to modify almost every setting in that dictionary.
 Leave them as they are for now.


* You should now be able to run the sample service locally (at port 28865, by default):

```sh
$ gunicorn -k aiohttp.worker.GunicornWebWorker aio_hipchat_getting_started.app:app --bind 127.0.0.1:28865
```

You should see at whole lot of (non-error) log lines on the screen

`Ctrl-C` to kill that process

Moving to heroku
================

Now that you are able to run the service locally, let's work at getting it hosted publicly, so that HipChat
can start using it.

Heroku is a web service hosting site that allows you to run your applications in a publicly available,
managed environment. It allows free services (with limits), up to full scale, high performance hosting for $$$.

You can easily choose a different service for your hosting (Rackspace, AWS). Obviously, these instructions will be
of limited value if you choose a different hosting.

Getting started
---------------

You'll need to setup a [Heroku account](https://signup.heroku.com/identity) (it's free).

Then you'll need to install the [Heroku Toolbelt](https://devcenter.heroku.com/articles/getting-started-with-python$set-up).

With those thing in place, login to heroku:

```sh
$ heroku login
```

Then create a new app in heroku:

```sh
$ heroku create
```

```
Creating lit-bastion-5032 in organization heroku... done, stack is cedar-14
http://lit-bastion-5032.herokuapp.com/ | https://git.heroku.com/lit-bastion-5032.git
Git remote heroku added
```

This should give several bits of output, including the name of the application that heroku created.

To run your service locally, try this:

```sh
$ heroku local
```

Your app should now be running on [localhost:28865](http://localhost:28865/).


Deploying to Heroku
-------------------

[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

```sh
$ git add . 
$ git commit -m 'Initial checkin'
$ git push heroku master
```

This should complete without errors. If it doesn't, you need to fix them.

Almost the last piece of information tells you the address of the public server. This is important.
Take note of it.

If you look at the logs of your application, (`heroku logs`) you will probably see a whole lot of errors like this:

```
Connecting to redis failed. Retrying in 1 seconds
```

Adding other services to Heroku
-------------------------------

Almost all services need somewhere to store data permanently. This sample uses MongoDb.

This sample also uses Redis for caching and publish/subscribe communications between server instances.

To add these services to your Heroku app, you need to run these commands:

```sh
$ heroku addons:create heroku-redis:hobby-dev
$ heroku addons:create mongolab:sandbox
$ heroku config:set MONGO_URL_ALTERNATE_ENV_VAR=MONGOLAB_URI
```

You can see the config for your application via the `config` command. 
The sample app needs a `BASE_URL` environment variable, which should be set
to the name of your heroku's url.

```sh
$ heroku config
$ heroku config:set BASE_URL=https://immense-dusk-9189.herokuapp.com/
```

Once this is complete, your application is running publicly. 

To see the log output from your application, you can use this command:

```sh
$ heroku logs -t
```

Your application should be running without any stack traces.

You should be able to see the output of server by opening a browser on your application, e.g. for example:
https://immense-dusk-9189.herokuapp.com/

# Documentation

For more information about using Python on Heroku, see these Dev Center articles:

- [Getting Started with Python on Heroku](https://devcenter.heroku.com/articles/getting-started-with-python) 

- [Python on Heroku](https://devcenter.heroku.com/categories/python)

Hooking up to HipChat
=====================

Once your service is running happily on Heroku (or somewhere else), you need to register it with HipChat.

You need to be a room or group admin to add an addon to HipChat.

[Describe process to install addon]

